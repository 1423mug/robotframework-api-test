*** Settings ***
Documentation  API Testing in Robot Framework
Library  RequestsLibrary
Library  Collections
Library  BuiltIn

*** Variables ***
${Base_URL}  https://robotframework.org
${csrfmiddlewaretoken}  CgN9rG7nIyI5gxfUbJuevOoaRs3IJprpMiOw9PBaLYcUArJ7XOkKaDgqGg7u5jH2

*** Test Cases ***
Do a GET Request and validate the response code and response body
    [documentation]  This test case verifies that the response code of the GET Request should be 200,
    ...  the response body contains the 'title' key with value as 'Robot Framework',
    ...  and the response body contains the key 'location_type'.
    [tags]  Smoke
    Create Session  antifrezzz  ${Base_URL}  verify=true
    ${response}=  GET On Session  antifrezzz  /
    Status Should Be  200  ${response}  #Check Status as 200
    ${resp_body}=  Convert To String    ${response.content}
    Should Contain    ${resp_body}    Robot Framework


Head accept type Request
    [documentation]  This test case verifies that the accept_type of the GET Request should be content
    ...  text/html; charset=utf-8. Just simple example parsing HEAD values. Without start session.
    [Tags]    Smoke
    ${accept_type}=    Set Variable    text/html; charset=utf-8
    ${headers}=    Create Dictionary    Title    ${accept_type}
    ${response_headers}=    HEAD    ${Base_URL}  headers=${headers}
    ${content_type_response}=    Get From Dictionary    ${response_headers.headers}    Content-Type
    Should Be Equal    ${accept_type}    ${content_type_response}


Logon on conf
    [documentation]  This test case verifies error code of the POST Request.
    ...  Forbidden reason normal status. Just simple example POST method. Without start session.
    [Tags]    Smoke
    ${body}=    Create Dictionary   access_code=truecode123   csrfmiddlewaretoken=${csrfmiddlewaretoken}
    ${response}=    POST    https://tickets.robotframework.org/rc2022/exhibitor/login  json=${body}  expected_status=403
    Should Be Equal As Strings    Forbidden    ${response.reason}

